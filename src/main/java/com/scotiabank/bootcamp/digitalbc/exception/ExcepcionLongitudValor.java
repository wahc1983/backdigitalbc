package com.scotiabank.bootcamp.digitalbc.exception;

public class ExcepcionLongitudValor extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ExcepcionLongitudValor(String message) {
        super(message);
    }
}
