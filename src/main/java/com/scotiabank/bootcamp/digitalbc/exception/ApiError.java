package com.scotiabank.bootcamp.digitalbc.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Data
public class ApiError {
    private String date;
    private HttpStatus status;
    private String message;
    private List<String> errors;

    public ApiError(String date, HttpStatus status, String message, List<String> errors) {
        super();
        this.date = date;
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public ApiError(String date, HttpStatus status, String message, String error) {
        super();
        this.date = date;
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }

    public ApiError(String date, HttpStatus status, String message) {
        super();
        this.date = date;
        this.status = status;
        this.message = message;
    }
}
