package com.scotiabank.bootcamp.digitalbc.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.scotiabank.bootcamp.digitalbc.model.BadResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.io.IOException;
import java.time.OffsetDateTime;


@ControllerAdvice
@Slf4j
public class ExceptionControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionControllerAdvice.class);

    @ExceptionHandler(ResourceNotFoundException.class)
    protected ResponseEntity<BadResponse> handleResourceNotFoundHandling(ResourceNotFoundException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.NOT_FOUND, exception, request);
    }

    @ExceptionHandler(ApiException.class)
    protected ResponseEntity<BadResponse> handleApiException(ApiException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.NOT_FOUND, exception, request);
    }

    @ExceptionHandler(ResourceBadRequestException.class)
    protected ResponseEntity<BadResponse> handleResourceBadRequest(ResourceBadRequestException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<BadResponse> handleMethodArgumentValid(MethodArgumentNotValidException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    protected ResponseEntity<BadResponse> handleMissingServletRequestParameter(MissingServletRequestParameterException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<BadResponse> handleConstraintViolationException(ConstraintViolationException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<BadResponse> handleIllegalArgumentException(IllegalArgumentException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(ValidationException.class)
    protected ResponseEntity<BadResponse> handleValidationException(ValidationException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(InvalidFormatException.class)
    protected ResponseEntity<BadResponse> handleInvalidFormatException(InvalidFormatException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(ConversionFailedException.class)
    protected ResponseEntity<BadResponse> handleConversionFailedException(ConversionFailedException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(IOException.class)
    protected ResponseEntity<BadResponse> handleIOException(IOException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<BadResponse> handleResourceMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    @ExceptionHandler(ResourceInternalServerErrorException.class)
    protected ResponseEntity<BadResponse> handleResourceInternalServerErrorException(ResourceInternalServerErrorException exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, exception, request);
    }

    @ExceptionHandler
    protected ResponseEntity<BadResponse> handleGlobalExceptionHandling(Exception exception, WebRequest request) {
        return buildResponseEntity(HttpStatus.BAD_REQUEST, exception, request);
    }

    private ResponseEntity<BadResponse> buildResponseEntity(HttpStatus status, Exception exc, WebRequest request) {
        BadResponse apiError = getMessageResponse(status, exc, request);
        LOGGER.error(getMessageLogger(status, exc));
        return new ResponseEntity<>(apiError, status);
    }

    private BadResponse getMessageResponse(HttpStatus status, Exception exc, WebRequest request) {
        return new BadResponse()
                .timestamp(OffsetDateTime.now())
                .status(status.name())
                .error(exc.getMessage())
                .message(request.getDescription(false));
    }

    private String getMessageLogger(HttpStatus status, Exception exc) {
        return String.format("Date: %s - StatusCode: %s - StatusValue: %s - Error: %s ", OffsetDateTime.now(), status.value(), status.name(), exc.getMessage());
    }
}
