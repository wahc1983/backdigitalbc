package com.scotiabank.bootcamp.digitalbc.util;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Utils {
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter FORMATTER_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    public enum VALITE_DOC_TYPE {CC, CE, NIP, NIT, TI, PAP}

    public static LocalDate getParseStringToLocalDate(String date) {
        return LocalDate.parse(date, FORMATTER);
    }

    public static OffsetDateTime getParseStringToOffsetDateTime(String date) {
        return ZonedDateTime.parse(date, FORMATTER_TIME).toOffsetDateTime();
    }

    public static String getParseLocalDateToString(LocalDate date) {
        return date.format(FORMATTER);
    }

}
