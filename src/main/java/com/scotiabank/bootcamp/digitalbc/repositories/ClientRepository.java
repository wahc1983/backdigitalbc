package com.scotiabank.bootcamp.digitalbc.repositories;

import com.scotiabank.bootcamp.digitalbc.model.Clients;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Clients, Long> {

    @Query("SELECT c FROM Clients c WHERE ((c.documentType=?1 AND c.documentNumber=?2 AND c.documentIssueDate=?3) OR (c.name=?4 AND c.lastname=?5 AND c.birthDate=?6) OR c.email=?7 OR c.cellphone=?8)")
    Optional<Clients> findByClientParam(String docType, BigDecimal docNum, String issueDate, String name, String lastName, String birthDate, String email, String cellPhone);

}
