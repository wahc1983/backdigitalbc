package com.scotiabank.bootcamp.digitalbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalBcApplication {
    public static void main(String[] args) {
        SpringApplication.run(DigitalBcApplication.class, args);
    }
}
