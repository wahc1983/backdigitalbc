package com.scotiabank.bootcamp.digitalbc.controller;

import com.scotiabank.bootcamp.digitalbc.api.ClientApi;
import com.scotiabank.bootcamp.digitalbc.model.Clients;
import com.scotiabank.bootcamp.digitalbc.model.SendResponse;
import com.scotiabank.bootcamp.digitalbc.service.ClientService;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Generated;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static com.scotiabank.bootcamp.digitalbc.util.Utils.getParseLocalDateToString;
import static com.scotiabank.bootcamp.digitalbc.util.Utils.getParseStringToLocalDate;

@RestController
@Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-09-13T15:56:53.584Z[GMT]")
public class RestrictiveLists implements ClientApi {

    @Autowired
    private ClientService clientService;

    private static final Logger log = LoggerFactory.getLogger(RestrictiveLists.class);
    private final HttpServletRequest request;
    private Map<String, Object> response;

    @Autowired
    public RestrictiveLists(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public ResponseEntity<SendResponse> clientByParams(@NotNull @Size(max = 3) @ApiParam(value = "The document_type for Client", required = true, allowableValues = "CC, CE, NIP, NIT, TI") @Valid @RequestParam(value = "document_type", required = true) String documentType,
                                                        @NotNull @Size(max = 10) @ApiParam(value = "The document_number for Client", required = true) @Valid @RequestParam(value = "document_number", required = true) BigDecimal documentNumber,
                                                        @NotNull @Size(max = 12) @ApiParam(value = "The name for Client", required = true) @Valid @RequestParam(value = "name", required = true) String name,
                                                        @NotNull @Size(max = 12) @ApiParam(value = "The lastname for Client", required = true) @Valid @RequestParam(value = "lastname", required = true) String lastname,
                                                        @NotNull @ApiParam(value = "The document_issue_date for Client, Start date in format YYYY-MM-DD", required = true) @Valid @RequestParam(value = "document_issue_date", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate documentIssueDate,
                                                        @NotNull @ApiParam(value = "The birth_date for Client, Start date in format YYYY-MM-DD", required = true) @Valid @RequestParam(value = "birth_date", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate birthDate,
                                                        @NotNull @Pattern(regexp = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$") @Size(max = 45) @ApiParam(value = "The email for Client", required = true) @Valid @RequestParam(value = "email", required = true) String email,
                                                        @NotNull @Pattern(regexp = "^(?:(?:\\(?(?:00|\\+)([1-4]\\d\\d|[1-9]\\d?)\\)?)?[\\-\\.\\ \\\\/]?)?((?:\\(?\\d{1,}\\)?[\\-\\.\\ \\\\/]?){0,})(?:[\\-\\.\\ \\\\/]?(?:#|ext\\.?|extension|x)[\\-\\.\\ \\\\/]?(\\d+))?$") @Size(max = 25) @ApiParam(value = "The cellphone for Client", required = true) @Valid @RequestParam(value = "cellphone", required = true) String cellphone) {

        response = new HashMap<>();
        String accept = request.getHeader("Accept");

        if (accept != null && accept.contains("/")) {

            Clients client = getClient(documentType, documentNumber, name, lastname, documentIssueDate, birthDate, email, cellphone);

            if (clientService.findByClient(client)) {
                response = clientService.responseClient(client, "SI", "El Cliente existe");
            } else {
                response = clientService.responseClient(client, "NO", "El Cliente no existe");
            }

        }

        return new ResponseEntity<SendResponse>(getRespuesta(), HttpStatus.OK);
    }

    private Clients getClient(String documentType, BigDecimal documentNumber, String name, String lastname, LocalDate documentIssueDate, LocalDate birthDate, String email, String cellphone) {
        return new Clients()
                .documentType(documentType)
                .documentNumber(documentNumber)
                .name(name)
                .lastname(lastname)
                .documentIssueDate(getParseLocalDateToString(documentIssueDate))
                .birthDate(getParseLocalDateToString(birthDate))
                .email(email)
                .cellphone(cellphone);
    }

    private SendResponse getRespuesta() {
        return new SendResponse()
                .date(getParseStringToLocalDate(response.get("date").toString()))
                .clientReported(response.get("client_reported").toString())
                .status(response.get("status").toString())
                .message(response.get("mensaje").toString());
    }
}
