package com.scotiabank.bootcamp.digitalbc.model;

import com.scotiabank.bootcamp.digitalbc.util.Utils;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

import static com.scotiabank.bootcamp.digitalbc.exception.ValidadorArgumento.*;

/**
 *
 */
@Data
@Entity
@Validated
@Table(name = "client")
public class Clients implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final int LONGITUD_MINIMA_NOMBRE = 12;
    private static final int LONGITUD_MINIMA_APELLIDOS = 12;
    private static final int LONGITUD_MINIMA_DOC_NUMBER = 10;
    private static final int LONGITUD_MINIMA_DOC_TYPE = 3;
    private static final int LONGITUD_MINIMA_CELLPHONE = 24;
    private static final int LONGITUD_MINIMA_EMAIL = 45;

    private static final String MENSAJES_VALIDATOR_EMAIL = "Debe ingresar un email valido";
    private static final String VALIDATOR_EMAIL_REGEX = "(^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$)?";

    private static final String MENSAJES_VALIDATOR_CELLPHONE = "Debe ingresar un número celular valido";
    private static final String VALIDATOR_CELLPHONE_REGEX = "^(?:(?:\\(?(?:00|\\+)([1-4]\\d\\d|[1-9]\\d?)\\)?)?[\\-\\.\\ \\\\\\/]?)?((?:\\(?\\d{1,}\\)?[\\-\\.\\ \\\\\\/]?){0,})(?:[\\-\\.\\ \\\\\\/]?(?:#|ext\\.?|extension|x)[\\-\\.\\ \\\\\\/]?(\\d+))?$";

    private static final String MENSAJES_CAMPO_VALIDATOR_DOC_TYPE = "Tipo documento no valido %s";
    private static final String MENSAJES_CAMPO_LONGITUD_DOC_TYPE = "EL tipo documento debe tener una longitud menor o igual a %s";
    private static final String MENSAJES_CAMPO_LONGITUD_DOC_NUMBER = "EL documento de identificación debe tener una longitud menor o igual a %s";
    private static final String MENSAJES_CAMPO_VALIDATOR_DOC_NUMBER = "EL documento de identificación debe ser solo números";
    private static final String MENSAJES_CAMPO_LONGITUD_NOMBRES = "EL nombre debe tener una longitud menor o igual a %s";
    private static final String MENSAJES_CAMPO_LONGITUD_APELLIDOS = "EL apellido debe tener una longitud menor o igual a %s";
    private static final String MENSAJES_CAMPO_LONGITUD_EMAIL = "EL email debe tener una longitud menor o igual a %s";
    private static final String MENSAJES_CAMPO_LONGITUD_CELLPHONE = "EL telefono debe tener una longitud menor o igual a %s";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "document_type")
    private String documentType;

    @Column(name = "document_number")
    private BigDecimal documentNumber;

    private String name;
    private String lastname;

    @Column(name = "document_issue_date")
    private String documentIssueDate;

    @Column(name = "birth_date")
    private String birthDate;

    private String email;
    private String cellphone;


    public Clients documentType(String documentType) {
        validarLongitudMinima(documentType, LONGITUD_MINIMA_DOC_TYPE, String.format(MENSAJES_CAMPO_LONGITUD_DOC_TYPE, LONGITUD_MINIMA_DOC_TYPE));
        validarValido(documentType, Utils.VALITE_DOC_TYPE.class, String.format(MENSAJES_CAMPO_VALIDATOR_DOC_TYPE, documentType));
        this.documentType = documentType;
        return this;
    }

    public Clients documentNumber(BigDecimal documentNumber) {
        validarRegex(documentNumber.toString(), "[0-9]*", MENSAJES_CAMPO_VALIDATOR_DOC_NUMBER);
        validarLongitudMinima(documentNumber, LONGITUD_MINIMA_DOC_NUMBER, String.format(MENSAJES_CAMPO_LONGITUD_DOC_NUMBER, LONGITUD_MINIMA_DOC_NUMBER));
        this.documentNumber = documentNumber;
        return this;
    }

    public Clients name(String name) {
        validarLongitudMinima(name, LONGITUD_MINIMA_NOMBRE, String.format(MENSAJES_CAMPO_LONGITUD_NOMBRES, LONGITUD_MINIMA_NOMBRE));
        this.name = name;
        return this;
    }

    public Clients lastname(String lastname) {
        validarLongitudMinima(lastname, LONGITUD_MINIMA_APELLIDOS, String.format(MENSAJES_CAMPO_LONGITUD_APELLIDOS, LONGITUD_MINIMA_APELLIDOS));
        this.lastname = lastname;
        return this;
    }

    public Clients documentIssueDate(String documentIssueDate) {
        this.documentIssueDate = documentIssueDate;
        return this;
    }

    public Clients birthDate(String birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public Clients email(String email) {
        validarRegex(email, VALIDATOR_EMAIL_REGEX, MENSAJES_VALIDATOR_EMAIL);
        validarLongitudMinima(email, LONGITUD_MINIMA_EMAIL, String.format(MENSAJES_CAMPO_LONGITUD_EMAIL, LONGITUD_MINIMA_EMAIL));
        this.email = email;
        return this;
    }

    public Clients cellphone(String cellphone) {
        validarRegex(cellphone, VALIDATOR_CELLPHONE_REGEX, MENSAJES_VALIDATOR_CELLPHONE);
        validarLongitudMinima(cellphone, LONGITUD_MINIMA_CELLPHONE, String.format(MENSAJES_CAMPO_LONGITUD_CELLPHONE, LONGITUD_MINIMA_CELLPHONE));
        this.cellphone = cellphone;
        return this;
    }
}
