package com.scotiabank.bootcamp.digitalbc.service;

import com.scotiabank.bootcamp.digitalbc.model.Clients;
import com.scotiabank.bootcamp.digitalbc.repositories.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public boolean findByClient(Clients client) {
        return clientRepository.findByClientParam(client.getDocumentType(), client.getDocumentNumber(), client.getDocumentIssueDate(),
                        client.getName(), client.getLastname(), client.getBirthDate(), client.getEmail(), client.getCellphone())
                .isPresent();
    }

    public Map<String, Object> responseClient(Clients client, String... msg) {
        Map<String, Object> response = new HashMap<>();
        response.put("date", LocalDate.now().toString());
        response.put("status", "OK");
        response.put("client_reported", msg[0]);
        response.put("mensaje", msg[1]);
        response.put("transaccion", client);
        return response;
    }
}
