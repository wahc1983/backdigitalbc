package com.scotiabank.bootcamp.digitalbc.service;

import com.scotiabank.bootcamp.digitalbc.model.Clients;

import java.util.Map;

public interface ClientService {

    boolean findByClient(Clients client) ;

    Map<String, Object> responseClient(Clients client, String... params);
}
