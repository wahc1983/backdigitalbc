package com.scotiabank.bootcamp.digitalbc;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DigitalBcApplicationTests {

	@Test
	@Disabled
	void contextLoads() {
	}

}
